<?php
require_once 'core.php';
require_once 'mailchimp.php';

function errorsSentry(){
  #return new Raven_Client(SENTRY_URI);
}

function db(){
  $client = errorsSentry();
  try{
    $conn = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME,"3306");
    return $conn;
  }catch(Exception $ex){
   # $client->getIdent($client->captureException($ex,array("level"=>'error')));
  }
}

function getTicket(){
  $client = errorsSentry();
  try{
    $url =
    'https://cerodeudasadecvrfccde110622esa.quickbase.com/db/main?act=API_Authenticate&username='.USERNAME.'&password='.PASSWORD.'&hours=24';
    $XML = simplexml_load_string(file_get_contents($url));
    $ticket = (string) $XML->ticket;
    return $ticket;
  }catch(Exception $ex){
    #$client->getIdent($client->captureException($ex,array("level"=>'error')));
  }
}

function dateQB($fecha_qb){
  $client = errorsSentry();
  try{
    if ($fecha_qb != "") {
      $epoch = substr(($fecha_qb),0,10);
      $dt = new DateTime("@$epoch");
      $dia = $dt->format("d");
      $mes = $dt->format("m");
      $anio = $dt->format("Y");
      $fecha_inicio = $anio."/".$mes."/".$dia;
      return $fecha_inicio;
    }else{
      return $fecha_qb;
    }
  }catch(Exception $ex){
    #$client->getIdent($client->captureException($ex,array("level"=>'error')));
  }
}

function dateQBMD($fecha_qb){
  $client = errorsSentry();
  try{
    if ($fecha_qb != "") {
      $epoch = substr(($fecha_qb),0,10);
      $dt = new DateTime("@$epoch");
      $dia = $dt->format("d");
      $mes = $dt->format("m");
      $anio = $dt->format("Y");
      $fecha_inicio = $mes."/".$dia;
      return $fecha_inicio;
    }else{
      return $fecha_qb;
    }
  }catch(Exception $ex){
    #$client->getIdent($client->captureException($ex,array("level"=>'error')));
  }
}

function toPercent($por){
  $client = errorsSentry();
  try{
    $por2 = (floatval($por))*100;
    return $por2."%";
  }catch(Exception $ex){
     # $client->getIdent($client->captureException($ex,array("level"=>'error')));
  }
}

function toPercentN($por){
  $client = errorsSentry();
  try{
    return (floatval($por))*100;
  }catch(Exception $ex){
    #$client->getIdent($client->captureException($ex,array("level"=>'error')));
  }
}

function toMoney($moneda){
  $client = errorsSentry();
  try{
    $moneda = floatval($moneda);
    $result = "$".number_format($moneda,2);
    return $result;
  }catch(Exception $ex){
    #$client->getIdent($client->captureException($ex,array("level"=>'error')));
  }
}

function getClientesToMailchimp($list,$reporte){
  $client = errorsSentry();
  try{
    $ticket = getTicket();
    $url = "https://cerodeudasadecvrfccde110622esa.quickbase.com/db/bf7gvnwyy?a=API_DoQuery&qid=".$reporte."&ticket=".$ticket."&apptoken=".TOKEN;
    $XML = simplexml_load_string(file_get_contents($url)) or die("Error");
    if ($XML->errtext == 'No error') {
      foreach($XML->record as $key => $r){
        if (($r->related_oportunidade___correo_lead) != '') {
          getData($r->related_oportunidade___correo_lead,$r,$list);
        }
        if (($r->related_oportunidade___correo_2) != '') {
          getData($r->related_oportunidade___correo_2,$r,$list);
        }
        if (($r->related_oportunidade___correo_3) != '') {
          getData($r->related_oportunidade___correo_3,$r,$list);
        }
      }
    }
  }catch(Exception $ex){
    #$client->getIdent($client->captureMessage($ex,array("level"=>'error')));
  }
}

function getClientesToMailchimpNotificaciones($list,$reporte){
  $client = errorsSentry();
  try{
    $ticket = getTicket();
    $url = "https://cerodeudasadecvrfccde110622esa.quickbase.com/db/bf7gvnwyy?a=API_DoQuery&qid=".$reporte."&ticket=".$ticket."&apptoken=".TOKEN;
    $XML = simplexml_load_string(file_get_contents($url)) or die("Error");
    if ($XML->errtext == 'No error') {
      foreach($XML->record as $key => $r){
        if (($r->related_oportunidade___correo_lead) != '') {
          getDataNotificaciones($r->related_oportunidade___correo_lead,$r,$list);
        }
        if (($r->related_oportunidade___correo_2) != '') {
          getDataNotificaciones($r->related_oportunidade___correo_2,$r,$list);
        }
        if (($r->related_oportunidade___correo_3) != '') {
          getDataNotificaciones($r->related_oportunidade___correo_3,$r,$list);
        }
      }
    }
  }catch(Exception $ex){
    #$client->getIdent($client->captureMessage($ex,array("level"=>'error')));
  }
}

function getClientesToMailchimpOportunidades($list,$reporte){
  $client = errorsSentry();
  try{
    $ticket = getTicket();
    $url = "https://cerodeudasadecvrfccde110622esa.quickbase.com/db/bf7gvnwv2?a=API_DoQuery&qid=".$reporte."&ticket=".$ticket."&apptoken=".TOKEN;
    $XML = simplexml_load_string(file_get_contents($url)) or die("Error");
    if ($XML->errtext == 'No error') {
      foreach($XML->record as $key => $r){
          getDataOportunidades($r->correo_lead,$r,$list);
      }
    }
  }catch(Exception $ex){
    #$client->getIdent($client->captureMessage($ex,array("level"=>'error')));
  }
}

function getData($email,$r,$list){
  $client = errorsSentry();
  try{
    $mailchimp = new mailchimp(MAILCHIMP_API_KEY);
    $metodo = "PUT";
    $ruta = "lists/".$list."/members"."/".md5((string)$email);
    if(!checkIfExists($list,$email)){
      $metodo = "POST";
      $ruta = "lists/".$list."/members";
    }
    $params = array(
                      array("method" => $metodo,
                            "path" => "lists/".$list."/members"."/".md5((string)$email),
                            "body" => "{\"email_address\":\"$email\",
                                        \"merge_fields\":{
                                            \"FNAME\":\"\",
                                            \"LNAME\":\"\",
                                            \"MMERGE3\":\"$r->oportunidade___record_id___solicitud____nombre_s_\",
                                            \"MMERGE4\":\"$r->oportunidade___dias_de_deposito\",
                                            \"MMERGE6\":\"".toMoney($r->related_oportunidade___apartado_mensual)."\",
                                            \"MMERGE7\":\"$r->meses_en_el_programa\",
                                            \"MMERGE8\":\"".dateQB($r->oportunidade___fecha_de_inicio_en_el_programa)."\",
                                            \"MMERGE9\":\"".dateQB($r->fecha_de_baja)."\",
                                            \"MMERGE10\":\"".dateQBMD($r->related_oportunidade___record_id__solicitud___fecha_de_nacimiento)."\",
                                            \"MMERGE11\":\"$r->razon_baja__nuevo_\",
                                            \"MMERGE12\":\"$r->no__apartados_realizados\",
                                            \"MMERGE13\":\"".toMoney($r->total_ahorrado_a_la_fecha)."\",
                                            \"MMERGE15\":\"$r->meses_sin_ahorrar_consecutivos\",
                                            \"MMERGE16\":\"".toPercent($r->descuento_final)."\",
                                            \"MMERGE5\":\"$r->___ahorro_requerido\",
                                            \"MMERGE14\":\"".toMoney($r->total_deuda_inicial)."\",
                                            \"MMERGE18\":\"$r->tipo_recomendador\",
                                            \"MMERGE17\":\"$r->___recomendados\"},
                                        \"status\":\"subscribed\"}"
                          )
                        );
                        #___recomendados
    $batch = $mailchimp->POST_batches_collection($params);
    var_dump($batch);
  }catch(Exception $ex){
    #$client->getIdent($client->captureException($ex,array("level"=>'error')));
  }
}

function getDataNotificaciones($email,$r,$list){
  $client = errorsSentry();
  try{
    $mailchimp = new mailchimp(MAILCHIMP_API_KEY);
    $metodo = "PUT";
    $ruta = "lists/".$list."/members"."/".md5((string)$email);
    if(!checkIfExists($list,$email)){
      $metodo = "POST";
      $ruta = "lists/".$list."/members";
    }
    $params = array(
                      array("method" => $metodo,
                            "path" => "lists/".$list."/members"."/".md5((string)$email),
                            "body" => "{\"email_address\":\"$email\",
                                        \"merge_fields\":{
                                            \"FNAME\":\"\",
                                            \"LNAME\":\"\",
                                            \"MMERGE3\":\"$r->oportunidade___record_id___solicitud____nombre_s_\",
                                            \"MMERGE4\":\"$r->oportunidade___dias_de_deposito\",
                                            \"MMERGE6\":\"".toMoney($r->related_oportunidade___apartado_mensual)."\",
                                            \"MMERGE7\":\"$r->meses_en_el_programa\",
                                            \"MMERGE8\":\"".dateQB($r->oportunidade___fecha_de_inicio_en_el_programa)."\",
                                            \"MMERGE9\":\"".dateQB($r->fecha_de_baja)."\",
                                            \"MMERGE10\":\"".dateQBMD($r->related_oportunidade___record_id__solicitud___fecha_de_nacimiento)."\",
                                            \"MMERGE11\":\"$r->razon_baja__nuevo_\",
                                            \"MMERGE12\":\"$r->no__apartados_realizados\",
                                            \"MMERGE13\":\"".toMoney($r->total_ahorrado_a_la_fecha)."\",
                                            \"MMERGE15\":\"$r->meses_sin_ahorrar_consecutivos\",
                                            \"MMERGE16\":\"".toPercent($r->descuento_final)."\",
                                            \"MMERGE5\":\"$r->___ahorro_requerido\",
                                            \"MMERGE14\":\"".toMoney($r->total_deuda_inicial)."\",
                                            \"MMERGE17\":\"\",
                                            \"MMERGE19\":\"$r->tipo_recomendador\",
                                            \"MMERGE18\":\"$r->___recomendados\"},
                                        \"status\":\"subscribed\"}"
                          )
                        );
                        #___recomendados
    $batch = $mailchimp->POST_batches_collection($params);
    var_dump($batch);
  }catch(Exception $ex){
    #$client->getIdent($client->captureException($ex,array("level"=>'error')));
  }
}

function getDataOportunidades($email,$r,$list){
  $client = errorsSentry();
  try{
    $mailchimp = new mailchimp(MAILCHIMP_API_KEY);
    $metodo = "PUT";
    $ruta = "lists/".$list."/members"."/".md5((string)$email);
    if(!checkIfExists($list,$email)){
      $metodo = "POST";
      $ruta = "lists/".$list."/members";
    }
    $params = array(
                      array("method" => $metodo,
                            "path" => $ruta,
                            "body" => "{\"email_address\":\"$email\",
                                        \"merge_fields\":{
                                            \"MMERGE4\":\"".dateQB($r->date_created)."\",
                                            \"MMERGE5\":\"$r->origen_de_oportunidad__nuevo_\",
                                            \"MMERGE6\":\"".toPercentN($r->maximo_porcentaje_del_avance)."\",
                                            \"MMERGE7\":\"$r->status_de_la_oportunidad\",
                                            \"MMERGE2\":\"$r->razon_de_desecho\",
                                            \"MMERGE3\":\"$r->razon_no_perfila\",
                                            \"MMERGE8\":\"$r->___of_clientes\",
                                            \"MMERGE9\":\"$r->saldo_disponible_actinver\",
                                            \"MMERGE1\":\"$r->performance_mkt\"},
                                        \"status\":\"subscribed\"}"
                          )
                        );
    $batch = $mailchimp->POST_batches_collection($params);
    var_dump($batch);
  }catch(Exception $ex){
    #$client->getIdent($client->captureException($ex,array("level"=>'error')));
  }
}

function checkIfExists($list,$email){
  $response = false;
  $mailchimp = new mailchimp(MAILCHIMP_API_KEY);
  $data = $mailchimp->GET_list_members_instance($list,$email);
  if(isset($data->id)){
      $response = true;
  }
  return $response;
}
