<?php
set_time_limit(0);
date_default_timezone_set("America/Mexico_City");
require_once '/var/projects/curadeuda-mailchimp/core/functions.php';
require_once '/var/projects/curadeuda-mailchimp/core/mailchimp.php';
$list = "483b94be07";

$mailchimp = new mailchimp(MAILCHIMP_API_KEY);

$data_list = $mailchimp->GET_lists_instance($list);
$member_count = $data_list->stats->member_count;
$db = db();
$db->query("TRUNCATE subscribers;");

for($i=1;$i<=$member_count;$i+=1000){
  $start = $i-1;
  $end = $i+1000;
  $batches = $mailchimp->GET_list_members_collection($list,$start,$end);
  foreach($batches as $b){
    foreach((array)$b as $x){
        $email_adrees = $x->email_address;
        $unique_email_id = $x->unique_email_id;
        $status = $x->status;
        if(isset($email_adrees)&&$email_adrees!=""){
          $sql = "INSERT INTO subscribers (email_address,unique_email_id,status,list) VALUES (";
          $sql.= '"'.$email_adrees.'","'.$unique_email_id.'","'.$status.'","'.$list.'"';
          $sql.= ");";
          $db->query($sql);
        }

    }
  }
}
